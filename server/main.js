import { Meteor } from 'meteor/meteor';
import '../imports/startup/server/fixtures.js';
import '../imports/api/server/publications.js';

Meteor.startup(() => {
});
