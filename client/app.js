import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

// routes
import '../imports/startup/client/routes/root/root.js';
import '../imports/startup/client/routes/timeline/timeline.js';

// configs
import '../imports/startup/client/config/webconfig.js';

// layouts
import '../imports/ui/layouts/main/mainLayout.js';

// templates layouts
import '../imports/ui/header/header.js';
import '../imports/ui/footer/footer.js';

// templates pages
import '../imports/ui/home/home.js';
import '../imports/ui/timeline/timeline.js';

// templates utils
import '../imports/ui/loader/loader.js';

