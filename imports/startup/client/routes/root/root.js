FlowRouter.route('/', {
  name: 'root',
  action: function() {
    BlazeLayout.render(
        'mainLayout', 
        { 
            main: 'home', 
        });
  }
});

FlowRouter.route('/home', {
  name: 'home',
  action: function() {
    BlazeLayout.render(
        'mainLayout', 
        { 
            main: 'home', 
        });
  }
});