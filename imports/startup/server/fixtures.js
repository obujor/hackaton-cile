import { Mongo } from 'meteor/mongo';
import { Years } from '../../api/years/years.js';
import { YearsShortTitles } from '../../api/years-short-titles/years-short-titles.js';
import { _ } from 'underscore-node';

Years.remove({});
const data = JSON.parse(Assets.getText('keywords.json'));
data.years.year.forEach(function(year){
  let yearArray = Object.keys(year).map(key => year[key])
  Years.insert(mapYear(year));
});

YearsShortTitles.remove({});
const dataShortTitles = JSON.parse(Assets.getText('mixed.json'));
dataShortTitles.years.year.forEach(function(year){
  let yearArray = Object.keys(year).map(key => year[key])
  YearsShortTitles.insert(mapYear(year));
});


function relevantKeyword(item) {
  return !( item.value == 'legislation' ||
            item.value == 'courts of law' ||
            item.value == 'local government');
};

function mapYear(year) {
  year.keywords = year.keywords || {};
  year.keywords.k = year.keywords.k || [];
  year.keywords.k = _.isArray(year.keywords.k) ? year.keywords.k: [year.keywords.k];
  year.keywords = year.keywords.k.map(function(keyword) {
    return {
      frequency: Math.round(keyword['frequency-in-year']),
      value: keyword['#text'],
      occurrencies : keyword['occurrencies']
    }
  }).filter(relevantKeyword);
  var maxFrequency = _.max(_.pluck(year.keywords, 'frequency'));
  year.topKeywords = _.where(year.keywords, {frequency: maxFrequency});
  year.keywords = _.difference(year.keywords, year.topKeywords);
  return year;
};