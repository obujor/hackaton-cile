import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Years } from '../../api/years/years.js';
import { YearsShortTitles } from '../../api/years-short-titles/years-short-titles.js';
import { _ } from 'underscore-node';
import './timeline.html';

Template.timeline.onCreated(function timelinOnCreated() {
  this.state = new ReactiveDict();
  const subscriptionHandle = this.subscribe('years-short-titles');
  
  this.state.set('start-year','1981');
  this.state.set('end-year','2016');
  this.state.set('year-to-see','1981');
  this.state.set('topic-to-see','');
  this.state.set('keywords-type','extracted from Akoma Ntoso "keyword" element')
  this.state.set('keywords-type-numeric','1')
});

Template.timeline.onRendered(function(){
  $('.modal-trigger').leanModal();
});
 
Template.timeline.helpers({
  years(){
    const instance = Template.instance();
    /*if (instance.state.get('keywords-type-numeric') == '1')
      return Years.find({
        value: {
          $gte : instance.state.get('start-year'),
          $lte : instance.state.get('end-year'),
          $ne : 'undated'
        }},{
        sort: {
          value: 1
        }
      });*/
    /*else if (instance.state.get('keywords-type-numeric') == '2')*/
    return YearsShortTitles.find({
      value: {
        $gte : instance.state.get('start-year'),
        $lte : instance.state.get('end-year'),
        $ne : 'undated'
      }},{
      sort: {
        value: 1
      }
    });
  },
  getDocumentsForYears(){
    const instance = Template.instance();
    const yearToSee = instance.state.get('year-to-see'); 
    let keywords = YearsShortTitles.findOne({"value":yearToSee});
    keywords = [].concat(keywords.topKeywords, keywords.keywords);
    return _.map(keywords, function(keyword) {
      keyword.occurrencies = _.uniq(keyword.occurrencies,function(occ){
        return occ.uri;
      });
      return keyword;
    });
  },
  getDocumentsForTopicYear(){
    const instance = Template.instance();
    const topic = instance.state.get('topic-to-see');
    const yearToSee = instance.state.get('year-to-see'); 
    const keywords = YearsShortTitles.findOne({"value":yearToSee});
    const keys = [].concat(keywords.topKeywords, keywords.keywords);
    const result = _.where(keys, {value: topic});
    return result;
  },
  getStartYear(){
    const instance = Template.instance();
    return instance.state.get('start-year');
  },
  getEndYear(){
    const instance = Template.instance();
    return instance.state.get('end-year');
  },
  getKeywordsType(){
    const instance = Template.instance();
    return instance.state.get('keywords-type');
  }
});

Template.timeline.events({
  'change #start-year'(event, instance){
    event.preventDefault();
    instance.state.set('start-year', event.target.value);
  },
  'change #end-year'(event, instance){
    event.preventDefault();
    instance.state.set('end-year', event.target.value);
  },
  'click #keywords-tags-radio'(event, instance){
    instance.state.set('keywords-type', 'extracted from Akoma Ntoso "keyword" element');
    instance.state.set('keywords-type-numeric', '1');
  },
  'click #keywords-short-titles-radio'(event, instance){
    instance.state.set('keywords-type', 'extracted with NLP from Akoma Ntoso "shortTitle" element');
    instance.state.set('keywords-type-numeric', '2');
  },
  'click .scroll-top-button'(event,instance){
    $('html, body').animate({
        scrollTop: $("#timeline-header").offset().top
    }, 2000);
  },
  'click .scroll-down-button'(event,instance){
    $('html, body').animate({
        scrollTop: $(".page-footer").offset().top
    }, 2000);
  },
  'click .thumb'(event,instance){
    instance.state.set('year-to-see',$(event.target).attr('data-year'));
    $('#modal2').openModal();
  },
  'click .chip'(event,instance){
    instance.state.set('year-to-see',$(event.target).attr('data-year'));
    instance.state.set('topic-to-see',$(event.target).attr('data-topic'));
    $('#modal3').openModal();
  }
});