import { Template } from 'meteor/templating';
 
import './home.html';

Template.home.onCreated(function timelinOnCreated() {
  Meteor.subscribe('years');
  Meteor.subscribe('years-short-titles');
});
 
Template.home.helpers({
});
