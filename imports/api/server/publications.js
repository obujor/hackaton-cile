import { Mongo } from 'meteor/mongo';
import { Years } from '../../api/years/years.js';
import { YearsShortTitles } from '../../api/years-short-titles/years-short-titles.js';

Meteor.publish('years', function yearsPublication() {
  return Years.find();
});
  
Meteor.publish('years-short-titles', function yearsShortTitlesPublication() {
  return YearsShortTitles.find();
});